#az login

echo "enter resource group name"
read nameRG

echo "enter app plan name"
read namePlan

echo "enter storage name"
read nameS

echo "enter application name"
read nameapp


echo "enter a name for your slot deployment"
read nameSlot



#Create a resource group

az group create \
 --name $nameRG \
  --location westus3

  #Create an app plan

  az functionapp plan create \
   -g $nameRG \
    -n $namePlan \
    --is-linux true \
     --min-instances 1 \
      --max-burst 10 \
       --sku EP1

#Create a general-purpose storage account

az storage account create \
 -g $nameRG \
 -n $nameS \
  -l westus3 \
   --kind StorageV2 \
   --sku Standard_LRS

#Create the function app in Azure

az functionapp create \
--resource-group $nameRG \
 --plan $namePlan \
  --name $nameapp \
  --storage-account $nameS \
   --functions-version 4 \
   --runtime dotnet

   #Configure appfunction to use "dotnet-isolated"

   az functionapp config appsettings set \
    --name $nameapp \
     --resource-group $nameRG \
      --settings "FUNCTIONS_WORKER_RUNTIME=dotnet-isolated"

sleep 5

      #Create azure function slot

      az functionapp deployment slot create \ 
        -n $nameapp \
        --resource-group $nameRG \
        --slot $nameSlot



