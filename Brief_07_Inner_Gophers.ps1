$group = Read-Host "Saisir groupe"
$storage = Read-Host "Saisir nom du storage"
$funcionapp = Read-Host "Saisir nom funtionapp"



 
###Create resource group
az group create --location eastus --name $group
 
###Create account storage
az storage account create --name $storage -g $group
 
###Create plan functionapp
az functionapp plan create --name testfunctionplan -g $group --skuEP1
 
###Create functionapp
az functionapp create --name $funcionapp -g $group -p testfunctionplan --storage-account $storage --functions-version 4 --os-type Linux --runtime dotnet
 
###Create slot deployement
az functionapp deployment slot create --name $funcionapp --resource-group $group --slot staging
 
###Modifier le slot dotnet en dotnet-isolated
 az functionapp config appsettings set --name $funcionapp --resource-group $group --settings "FUNCTIONS_WORKER_RUNTIME=dotnet-isolated"